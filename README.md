# Setup

Install the ruby version 2.7.0
```sh
$ rbenv install 2.7.0
$ gem install bundler -v '2.2.0'
```
Install dependencies.
```sh
$ bundle install
```
Create Database
```sh
$ rails db:create
```
Run Migrations
```sh
$ rails db:migrate
```

