class CreateRecords < ActiveRecord::Migration[6.0]
  def change
    create_table :records do |t|
      t.string 'name'
      t.string 'code'
      t.string 'city'
      t.string 'actor'
      t.string 'defendant'
      t.text 'summary'
      t.string 'court'
      t.string 'url'
      t.timestamps
    end
  end
end
