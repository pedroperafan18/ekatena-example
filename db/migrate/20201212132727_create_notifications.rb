class CreateNotifications < ActiveRecord::Migration[6.0]
  def change
    create_table :notifications do |t|
      t.belongs_to :record, foreign_key: true
      t.string 'date'
      t.text 'message'

      t.timestamps
    end
  end
end
