class RecordsController < ApplicationController
  def index
    @records = Record.last(5)
  end

  def show
    @record = Record.find(params[:id])
  end

  def create
    response = RecordCreatorService.call(params[:url])
    if response[:error].present?
      flash_message(response)
      redirect_to new_record_path
    else
      redirect_to record_url(response[:id])
    end
  end
end
