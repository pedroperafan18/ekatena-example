class ApplicationController < ActionController::Base
  def flash_message(response)
    flash[:success] = { data: response[:success] } if response[:success]
    flash[:error] = { data: response[:error] } if response[:error]
  end
end
