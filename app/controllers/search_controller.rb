class SearchController < ApplicationController
  before_action :redirect_no_query, only: %w[search]

  def search
    @records = Record.where('defendant LIKE ?', "%#{query}%")

    redirect_no_records if @records.count.zero?
  end

  private

  def query
    params[:name]
  end

  def redirect_no_query
    redirect_no_records if query.empty?
  end

  def redirect_no_records
    flash_message({ error: I18n.t('not_found_records')})
    redirect_to records_path
  end
end
