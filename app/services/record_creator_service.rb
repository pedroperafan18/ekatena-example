# frozen_string_literal: true

require 'open-uri'
require 'openssl'

class RecordCreatorService < ApplicationService
  REGEX_URL = /^(https|http):\/\/(www\.)?poderjudicialvirtual\.com\/[\w\d\/\-]+/

  def initialize(url)
    @url = url
    @record = Record.new
  end

  def call
    return { error: 'El expediente tiene que ser del sitio de poderjudicialvirtual.com' } unless valid_url?
    return { error: 'No es un expediente válido' } unless valid_record?

    record_data

    if @record.save
      notifications_data

      { id: @record.id }
    else
      { error: 'Ocurrió un error, intente de nuevo' }
    end
  end

  private

  def record_data
    @record.name = name
    @record.code = code
    @record.city = city
    @record.actor = actor
    @record.court = court
    @record.defendant = defendant
    @record.summary = summary
    @record.url = @url
  end

  def notifications_data
    notifications.each do |notification_data|
      @record.notifications.create(notification_data)
    end
  end

  def valid_url?
    @url.match(REGEX_URL)
  end

  def doc
    @doc ||= Nokogiri::HTML(URI.open(@url, { ssl_verify_mode: OpenSSL::SSL::VERIFY_NONE }))
  end

  def principal_container
    @principal_container ||= doc.css('#pcont')
  end

  def valid_record?
    principal_container.css('.header').count < 5
  end

  def content_container
    principal_container.css('.content').first.content
  end

  def header_container
    principal_container.css('.header').first.content
  end

  def summary_container
    principal_container.css('.content')[1].content
  end

  def notifications_container
    principal_container.css('#listaAcuerdos')
  end

  def info_content_container
    @info_content_container ||= info(content_container)
  end

  def info_header_container
    @info_header_container ||= info(header_container)
  end

  def info_summary_container
    @info_summary_container ||= info(summary_container)
  end

  def info(container)
    container.split("\r\n").map { |s| s.strip }.reject(&:blank?)
  end

  def code
    info_header_container.first.gsub(/.+Exp:./,'').strip
  end

  def name
    info_header_container.first.gsub(/.Exp:.+/,'').strip
  end

  def city
    info_content_container[0].strip
  end

  def court
    info_content_container[1].gsub(/>./,'').strip
  end

  def actor
    info_content_container[2].gsub(/Actor:./,'').gsub(/Demandado:.+/,'').strip
  end

  def defendant
    info_content_container[2].gsub(/.+Demandado:/,'').strip
  end

  def summary
    info_summary_container.first.gsub(/RESUMEN:/,'').strip
  end

  def notifications
    notifications_container.css('.list-group-item').map do |notification|
      { date: notification.css('.name').first.content, message: notification.css('.justify').first.content }
    end
  end
end
