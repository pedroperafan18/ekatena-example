Rails.application.routes.draw do
  root 'records#index'

  resources :records
  get '/search' => 'search#search', :as => 'search'
end
